package com.app.Sender;

import com.app.Domain.Student;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

@Component
public class Sender {

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private Queue queue;

    public void send(String studentMessage) {
        this.template.convertAndSend(queue.getName(), studentMessage);
        System.out.println(" [x] Sent '" + studentMessage + "'");
    }
}
