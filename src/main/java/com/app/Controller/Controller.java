package com.app.Controller;

import com.app.Domain.Student;
import com.app.Receiver.Receiver;
import com.app.Sender.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//creating RestController
@RestController
public class Controller
{
    //autowired the StudentService class
    @Autowired
    Receiver receiver;

    //autowired the Sender class
    @Autowired
    Sender sender;

    //creating a get mapping that retrieves all the students detail from the database
    @GetMapping("/student")
    private void receiveRequest()
    {
        receiver.receive("r2c ");
    }

    //creating post mapping that post the student detail in the database
    @PostMapping("/student")
    private int saveStudent(@RequestBody Student student)
    {
        //sending request
        sender.send(""+
                student.getId()+"-"+
                student.getAge()+"-"+
                student.getName()+"-"+
                student.getEmail());
        return 0;
    }
}
